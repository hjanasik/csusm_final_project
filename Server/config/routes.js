/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // '/': { view: 'pages/homepage' },

  /* Account */
  'POST /account/login':    { controller: 'Account/UserController', action: 'login' },
  'POST /account/register': { controller: 'Account/UserController', action: 'register' },
  'GET  /account/logout':   { controller: 'Account/UserController', action: 'logout' },
  'POST /account/checkAuthStatus': { controller: 'Account/UserController', action: 'checkAuthStatus' },

  /* Medicine */
  'POST /medicine': { controller: 'Medicine/MedicineController', action: 'add' },
  'GET  /medicine': { controller: 'Medicine/MedicineController', action: 'get' },
  'POST /deleteMedicine': { controller: 'Medicine/MedicineController', action: 'delete' }


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
