module.exports = {
    friendlyName: 'medicine existing test',
    description: 'Returns if the specified medicine of a user exists.',
    inputs: {
      uid: {
        type: 'string',
        description: 'The UID of the user to get the medicine from.',
        required: true,
      },
      medicineId: {
        type: 'string',
        description: 'The UID of the medicine to get from the user.',
        required: true,
      },
    },
    async fn(inputs, exits) {
      const medicineQuery = await db.collection('users').doc(inputs.uid).collection('medicines').doc(inputs.medicineId)
        .get();
      return exits.success(!!(medicineQuery && medicineQuery.data()));
    },
  };
  