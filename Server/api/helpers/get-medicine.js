module.exports = {
    friendlyName: 'Medicine getter',
    description: 'Returns the specified medicine from a user.',
    inputs: {
      uid: {
        type: 'string',
        description: 'The UID of the user to get the medicine from.',
        required: true,
      },
      medicineId: {
        type: 'string',
        description: 'The UID of the medicine to get from the user.',
        required: true,
      },
    },
    async fn(inputs, exits) {
      return exits.success(db.collection('users').doc(inputs.uid).collection('medicines').doc(inputs.medicineId));
    },
  };
  