module.exports = {
    friendlyName: 'User existing test',
    description: 'Returns if the specified user exists.',
    inputs: {
      uid: {
        type: 'string',
        description: 'The UID of the user',
        required: true,
      },
    },
    async fn(inputs, exits) {
      const userQuery = await db.collection('users').doc(inputs.uid).get();
      return exits.success(!!(userQuery && userQuery.data()));
    },
  };
  