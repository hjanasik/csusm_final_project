let fs = require('fs');
const http = require('http');
const fetch = require('node-fetch');

module.exports = {

    async add(req, res) {
        const uid = req.body.uid;
        let cip = req.body.cip;
        var jsonMedic;

        if (!(await sails.helpers.isUserExists(uid))) return res.json({ message: 'No corresponding user in database.', code: 'medicine/not-in-db' });
        if (!req.body.uid) return res.json({ message: 'No uid provided.', code: 'medicine/bad-request' });
        if (!req.body.cip) return res.json({ message: 'No cip provided.', code: 'medicine/bad-request' });

        async function getMedicFromApi() {
            const data = fs.readFileSync("./api/controllers/Medicine/CIS_CIP_bdpm.txt");
            const lines = data.toString().split(/\r?\n/);
            let cis;
            lines.forEach((line) => {
                if (line.includes(cip)) {
                    cis = line.substr(0, 8);
                }
            });
            let url = "http://localhost:3004/api/medicaments/" + cis;
            let settings = { method: "Get" };
            await fetch(url, settings)
                .then(response => response.json())
                .then((json) => {
                    jsonMedic = JSON.parse(JSON.stringify(json));
                    return (json);
                });
        }

        await getMedicFromApi();
        const userRef = db.collection('users').doc(uid);

        return userRef.get()
            .then(async () => {
                const id = await sails.helpers.firestoreGenerateId();
                const medicine = await sails.helpers.getMedicine(uid, id);

                return medicine.set({
                    name: jsonMedic.nom,
                    cis: jsonMedic.cis,
                    cip: cip,
                })
                .then(() => res.json({
                    message: `Medicine ${jsonMedic.nom} successfully added.`,
                    code: 'OK',
                    id,
                }))
                .catch((err) => {
                    console.error(err);
                    res.send(err);
                });
            });
    },

    async get(req, res) {
    let uid = req.query.uid;
    const medicines = [];
    const medicinesRef = db.collection('users').doc(uid).collection('medicines');
    const medicinesQuery = await medicinesRef.get();

    let ids = null;
    if (req.query && req.query.id) {
      ids = req.query.id.split(',');
    }

    // If the user does not exists
    if (!(await sails.helpers.isUserExists(uid))) return res.json({ message: 'No corresponding user in database.', code: 'medicine/not-in-db' });

    for (const medicineDoc of medicinesQuery.docs) {
      const medicine = medicineDoc.data();
      medicine.id = medicineDoc.id;
      const query = await medicinesRef.doc(medicineDoc.id).collection('medicines').get();
      query.forEach((medicDoc) => {
        const medic = medicDoc.data();
        medic.id = medicineDoc.id;
      });
      if (ids === null || (ids && ids.includes(medicineDoc.id))) {
        medicines.push(medicine);
      }
    }
    return res.json({ data: medicines, code: 'OK' });
  },

  async delete(req, res) {
    const uid = req.body.uid;

    // If the request is not valid
    if (!req.body.medicine_id) return res.json({ message: 'No medicine ID provided.', code: 'medicine/bad-request' });

    // If the user does not exists
    if (!(await sails.helpers.isUserExists(uid))) return res.json({ message: 'No corresponding user in database.', code: 'medicine/not-in-db' });

    // If the medicine does not exists
    if (!(await sails.helpers.isMedicinesExists(uid, req.body.medicine_id))) return res.json({ message: 'No corresponding medicine in database.', code: 'medicine/not-in-db' });

    const medicine = await sails.helpers.getMedicine(uid, req.body.medicine_id);
    const currentMedicine = await medicine.get()
      .then((doc) => (doc.exists ? doc.data() : null))
      .catch((err) => {
        console.error(err);
        res.send(err);
      });
    return medicine.delete()
      .then(() => res.json({ message: `Medicine "${currentMedicine.name}" successfully deleted.`, code: 'OK', id: req.body.medicine_id }))
      .catch((err) => {
        console.error(err);
        res.send(err);
      });
  },
};