const firebase = require('firebase');
const database = require('../../../db.js');
const admin = require('firebase-admin');

const db = database.db;
const auth = firebase.auth();

module.exports = {

  /* LOGIN */
  async login(req, res) {
    auth.setPersistence(firebase.auth.Auth.Persistence.NONE);
    auth.signInWithEmailAndPassword(req.body.email, req.body.password)
      .then((response) => response.user.getIdToken()
        .then((idToken) => {
          const expiresIn = 60 * 1000 * 60;
          admin.auth().createSessionCookie(idToken, { expiresIn })
            .then((sessionCookie) => {
              const options = {
                maxAge: expiresIn,
                httpOnly: true,
                sameSite: 'strict',
              };
              db.collection("users").doc(response.user.uid).get()
              .then((doc) => {
                const data = doc.data();
                res.cookie('__session', sessionCookie, options);
                auth.signOut();
                res.json({ data: { uid: response.user.uid, data }, code: 'OK' });
              })
              .catch((err) => {
                console.error(err);
                res.send(err);
              });
            })
            .catch((err) => {
              console.error(err);
              res.send(err);
            });
        }))
      .catch((err) => {
        console.error(err);
        res.send(err);
      });
  },

  /* REGISTER */
  async register(req, res) {
    auth.createUserWithEmailAndPassword(req.body.email, req.body.password)
      .then((response) => {
        db.collection('users').doc(response.user.uid).set({
          email: req.body.email,
          name: req.body.name,
          gender: req.body.gender
        })
          .catch((err) => {
            console.error(err);
            res.send(err);
          });
          console.log()
        res.json({ data: { uid: response.user.uid, email: req.body.email, name: req.body.name, gender: req.body.gender }, code: 'OK' });
      })
      .catch((err) => {
        console.error(err);
        res.send(err);
      });
  },

  async checkAuthStatus(req, res) {
    const { uid } = req;
    res.json({ data: { uid }, code: 'OK' });
  },

  /* LOGOUT */
  async logout(req, res) {
    auth.signOut()
      .then(() => {
        res.clearCookie('__session', {});
        res.json({ message: 'Successfully logged out.', code: 'OK' });
      })
      .catch((err) => {
        console.error(err);
        res.send(err);
      });
  },

}; // end of module.export