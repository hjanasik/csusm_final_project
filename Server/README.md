# Server

A nodeJS application. The server is part of a CSUSM Project


# Installation
Please first install [npm](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04-fr) and [nodeJS](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04-fr) on your computer.

# Usage

Go to the Server folder
```bash
cd Server
```

Download depedencies
```bash
npm i
```

Launch the server
```bash
npm start
```

## License
[MIT](https://choosealicense.com/licenses/mit/)