const firebase = require('firebase');


var firebaseConfig = {
    apiKey: "AIzaSyB-JRjYpJ4Yjg-BXhRRU10Hc56CqOOMhP4",
    authDomain: "csusm-project.firebaseapp.com",
    databaseURL: "https://csusm-project.firebaseio.com",
    projectId: "csusm-project",
    storageBucket: "csusm-project.appspot.com",
    messagingSenderId: "876511447769",
    appId: "1:876511447769:web:c8d588eef80b129597ccc8"
};

firebase.initializeApp(firebaseConfig);

const admin = require('firebase-admin');
const serviceAccount = require('./config/firebase/csusm-project-firebase-adminsdk-dilrq-81bc73feda.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

db = admin.firestore();

module.exports = { db, admin };