import 'package:Client/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  group("Login page", () {
    testWidgets('Find email form', (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());

      final _emailForm = find.byKey(Key("emailField"));
      expect(_emailForm, findsOneWidget);
    });
    testWidgets('Find password form', (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());

      final _passwordForm = find.byKey(Key("passwordField"));
      expect(_passwordForm, findsOneWidget);
    });
  testWidgets('Enter text in fields test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    final _emailForm = find.byKey(Key("emailField"));
    final _passwordForm = find.byKey(Key("passwordField"));
    // Verify that we have a email and password field.
    expect(_emailForm, findsOneWidget);
    expect(_passwordForm, findsOneWidget);

    await tester.enterText(_emailForm, "test@test.com");
    await tester.enterText(_passwordForm, "test1234");
  });
  testWidgets('try to connect test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());

    final _emailForm = find.byKey(Key("emailField"));
    final _passwordForm = find.byKey(Key("passwordField"));
    // Verify that we have a email and password field.
    expect(_emailForm, findsOneWidget);
    expect(_passwordForm, findsOneWidget);

    await tester.enterText(_emailForm, "test@test.com");
    await tester.enterText(_passwordForm, "test1234");

    await tester.tap(find.byType(RaisedButton));
  });

  testWidgets('Value field start empty', (WidgetTester tester) async {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    String fieldValue;

    Widget builder() {
      return MaterialApp(
        home: MediaQuery(
          data: const MediaQueryData(devicePixelRatio: 1.0),
          child: Directionality(
            textDirection: TextDirection.ltr,
            child: Center(
              child: Material(
                child: Form(
                  key: formKey,
                  child: TextFormField(
                    onSaved: (String value) { fieldValue = value; },
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }

    await tester.pumpWidget(builder());

    expect(fieldValue, isNull);
  });

  testWidgets('onSaved callback is called', (WidgetTester tester) async {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    String fieldValue;

    Widget builder() {
      return MaterialApp(
        home: MediaQuery(
          data: const MediaQueryData(devicePixelRatio: 1.0),
          child: Directionality(
            textDirection: TextDirection.ltr,
            child: Center(
              child: Material(
                child: Form(
                  key: formKey,
                  child: TextFormField(
                    onSaved: (String value) { fieldValue = value; },
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }

    await tester.pumpWidget(builder());

    expect(fieldValue, isNull);

    Future<void> checkText(String testValue) async {
      await tester.enterText(find.byType(TextFormField), testValue);
      formKey.currentState.save();

      expect(fieldValue, equals(testValue));
    }

    await checkText('Test');
    await checkText('');
  });

});
}