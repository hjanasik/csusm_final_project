// Special class of medicine for scanned Medicine
class ScannedMedicine {
  DateTime expirationDate;
  DateTime fabricationDate;

  String lotNb;
  String cip;
  String serialNb;

  // Transforms AAMMDD to DateTime
  _getDateFromString(String date) {
    final year = 2000 + int.parse(date.substring(0, 2));
    var month = int.parse(date.substring(2, 4));
    var day = int.parse(date.substring(4, 6));
    if (day == 0) {
      if (month < 12) {
        month++;
      } else {
        day = 31;
      }
    }

    return DateTime.utc(year, month, day);
  }

  ScannedMedicine()
      :expirationDate = null,
        fabricationDate = null,
        lotNb = '',
        cip = '',
        serialNb = '';

  ScannedMedicine.fromDataMatrix(String dataMatrix) {
    if (dataMatrix == null || dataMatrix.length < 27) {
      throw "Invalid dataMatrix";
    }
    // split dataMatrix in substrings with char FCN1, ASCII 29 askip
    for (var subMatrix in dataMatrix.split(String.fromCharCode(29))) {
      while (subMatrix.isNotEmpty) {
        var id = subMatrix.substring(0, 2);
        subMatrix = subMatrix.substring(2);
        switch (id) {
          case '01': // '0' + CIP13
            if (subMatrix.startsWith('0') && subMatrix.length >= 14) {
              cip = subMatrix.substring(1, 14);
              subMatrix = subMatrix.substring(14);
            } else {
              throw 'CIP Error';
            }
            break;
          case '17': // Expiration date AA/MM/JJ
            if (subMatrix.length < 6) {
              throw "Invalid expiration date";
            }
            expirationDate = _getDateFromString(subMatrix.substring(0, 6));
            subMatrix = subMatrix.substring(6);
            break;
          case '11': // Fabrication date AA/MM/JJ (optional)
            if (subMatrix.length < 6) {
              throw "Invalid fabrication date";
            }
            fabricationDate = _getDateFromString(subMatrix.substring(0, 6));
            subMatrix = subMatrix.substring(6);
            break;
          case '10': // Lot nb
            if (subMatrix.length > 20) {
              throw "Invalid lot number";
            }
            lotNb = subMatrix;
            subMatrix = '';
            break;
          case '21': // serial_nb (optional, usually at the end)
            serialNb = subMatrix;
            break;
          default: // Unknown id
            throw "Invalid dataMatrix: unknown id";
            break;
        }
      }
    }
  }
}