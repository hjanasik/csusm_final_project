import 'package:flutter/material.dart';
import 'package:Client/auth/login/auth_login_ui.dart';
import 'package:Client/auth/register/auth_register_ui.dart';
import 'package:Client/home.dart';
import 'package:Client/settings_screen.dart';
import 'package:Client/MedicineList.dart';

class Routes {
  Routes._();

  static const String login = '/login';
  static const String register = '/register';
  static const String home = '/home';

  static const String settings = '/settings';
  static const String cabinetDetail = '/cabinet-detail';
  static const String cabinet = '/cabinet';

  static final routes = <String, WidgetBuilder>{

    login: (BuildContext context) => SignInScreen(),
    register: (BuildContext context) => RegisterScreen(),

    home: (BuildContext context) => Home(),
    settings: (BuildContext context) => SettingScreen(),

    cabinet: (BuildContext context) => MedecineScreen(),
  };
}
