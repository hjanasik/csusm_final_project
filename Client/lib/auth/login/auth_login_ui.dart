import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Client/routes.dart';
import 'package:Client/Api.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  String _emailField = "";
  String _passwordField = "";
  bool buttonEnabled = false;
  bool validCredentials = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  double _height;
  double _width;

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(title: Text("Login")),
      body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: 20, bottom: 10),
            child: Column(
              children: <Widget>[
                FlutterLogo(size: 180,),
                form(),
                SizedBox(height: 20,),
                signInButton(context),
                SizedBox(height: 20,),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("No account : "),
                      InkWell(
                        child: Text("register here",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                        onTap: () =>
                            {Navigator.of(context).pushNamed(Routes.register)},
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: _width / 17.0, right: _width / 17.0, top: _height / 20.0),
      child: Form(
        key: _formKey,
        autovalidate: buttonEnabled,
        child: Column(
          children: <Widget>[
            emailField(),
            SizedBox(height: _height / 60.0),
            passwordField(),
            SizedBox(height: _height / 60.0),
            validCredentials
                ? Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 40),
                    child: Text("Invalid informations",
                      style: TextStyle(fontSize: 13, color: Colors.red),
                    ),
                  )
                : Text('')
          ],
        ),
      ),
    );
  }

  Widget signInButton(BuildContext context) {
    return RaisedButton(
      onPressed: () async {
        if (_emailField == "" || _passwordField == "")
          return setState(() { validCredentials = true;});
        
        var request = Api();
        if (await request.signin(_emailField, _passwordField))
           Navigator.pushNamedAndRemoveUntil(context, "/home", (r) => false);
        else
          return setState(() { validCredentials = true;});
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      textColor: Colors.white,
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
        width: _width / 1.2,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(40.0)),
          gradient: LinearGradient(
              colors: _emailField == "" || _passwordField == ""
                  ? <Color>[Colors.grey, Colors.grey]
                  : <Color>[Colors.deepPurple, Colors.purple]),
        ),
        padding: const EdgeInsets.all(15.0),
        child: Text("Login"),
      ),
    );
  }

  Widget emailField() {
    _width = MediaQuery.of(context).size.width;
    return Material(
      borderRadius: BorderRadius.circular(4),
      elevation: 20,
      color: Colors.transparent,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        key: Key("emailField"),
        cursorColor: Colors.purple,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.email, color: Colors.blue, size: 30),
            hintText: "email",),
        validator: validateEmail,
        onSaved: (val) => _emailField = val,
        onChanged: (value) {
          setState(() {
            _emailField = value;
          });
        },
      ),
    );
  }

  Widget passwordField() {
    _width = MediaQuery.of(context).size.width;

    return Material(
      borderRadius: BorderRadius.circular(4),
      elevation: 20,
      color: Colors.transparent,
      child: TextFormField(
        obscureText: true,
        keyboardType: TextInputType.emailAddress,
        autocorrect: false,
        key: Key("passwordField"),
        cursorColor: Colors.purple,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.lock, color: Colors.blue, size: 30),
          hintText: "password"),
        validator: (value) => value.isEmpty
            ? "Cannot be empty"
            : null,
        onChanged: (value) {
          setState(() {
            _passwordField = value;
          });
        },
        onSaved: (val) => _passwordField = val,
      ),
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    final regex = RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return "Enter your email";
    } else {
      return null;
    }
  }
}
