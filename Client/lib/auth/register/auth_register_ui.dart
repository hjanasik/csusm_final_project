import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Client/Api.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String _nameField = "";
  String _emailField = "";
  String _passwordField = "";
  String _cpasswordField = "";
  String dropdownValue = 'Man';
  bool buttonEnabled = false;
  bool validCredentials = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final key = GlobalKey<ScaffoldState>();
  double _height;
  double _width;

  void updateButton() {
    setState(() {
      buttonEnabled = _emailField != "" &&
          _nameField != "" &&
          _passwordField.length > 5 &&
          _cpasswordField != "";
    });
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return Material(
      child: Scaffold(
        key: key,
         appBar: AppBar(title: Text("Register")),
        body: Container(
            constraints: BoxConstraints.expand(),
            child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 20, bottom: 10),
              child: Column(
                children: <Widget>[
                  FlutterLogo(size: 140),
                  form(),
                  SizedBox(height: _height / 35),
                  buttonConfirm(),
                  SizedBox(height: _height / 35),
                ],
              ),
            ),
        ),
      ),
    );
  }

  Widget form() {
    return Container(
      margin: EdgeInsets.only(
          left: _width / 12.0, right: _width / 12.0, top: _height / 20.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            nameField(),
            SizedBox(height: _height / 60.0),
            emailField(),
            SizedBox(height: _height / 60.0),
            passwordField(),
            SizedBox(height: _height / 60.0),
            cpasswordField(),
            SizedBox(height: _height / 60.0),
            DropdownButton<String>(
              value: dropdownValue,
              isExpanded: true,
              icon: Icon(Icons.arrow_downward, color: Colors.blue,),
              iconSize: 24,
              elevation: 20,
              style: TextStyle(color: Colors.grey),
              underline: Container(
                height: 2,
                color: Colors.grey,
              ),
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue = newValue;
                });
              },
              items: <String>['Man', 'Woman', 'Other']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
            }).toList(),),
            validCredentials
                ? Text(
                    'Email or password invalid or email already used',
                    style: TextStyle(fontSize: 12, color: Colors.red),
                  )
                : Text(''),
          ],
        ),
      ),
    );
  }

  Widget nameField() {
    _width = MediaQuery.of(context).size.width;

    return Material(
      borderRadius: BorderRadius.circular(4),
      elevation: 20,
      color: Colors.transparent,
      key: Key("nameFieldRegister"),
      child: TextFormField(
        keyboardType: TextInputType.name,
        cursorColor: Colors.purple,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.account_circle, color: Colors.blue, size: 30),
            hintText:
                "Name",),
        validator: (value) => value.isEmpty
            ? "Enter your Name"
            : null,
        onChanged: (value) {
          _nameField = value;
          updateButton();
        },
      ),
    );
  }

  Widget emailField() {
    _width = MediaQuery.of(context).size.width;

    return Material(
      borderRadius: BorderRadius.circular(4),
      elevation: 20,
      color: Colors.transparent,
      key: Key("emailFieldRegister"),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        cursorColor: Colors.purple,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.email, color: Colors.blue, size: 30),
            hintText:
                "email",),
        validator: (value) => value.isEmpty
            ? "Enter your Email"
            : null,
        onChanged: (value) {
          _emailField = value;
          updateButton();
        },
      ),
    );
  }

  Widget passwordField() {
    _width = MediaQuery.of(context).size.width;

    return Material(
      borderRadius: BorderRadius.circular(4),
      elevation: 20,
      color: Colors.transparent,
      key: Key("passwordFieldRegister"),  
      child: TextFormField(
        obscureText: true,
        keyboardType: TextInputType.emailAddress,
        cursorColor: Colors.purple,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.lock, color: Colors.blue, size: 30),
            hintText:"password"),
        validator: (value) => value.isEmpty
            ? "Enter your password"
            : null,
        onChanged: (value) {
          _passwordField = value;
          updateButton();
        },
      ),
    );
  }

  Widget cpasswordField() {
    _width = MediaQuery.of(context).size.width;

    return Material(
      borderRadius: BorderRadius.circular(4),
      elevation: 20,
      color: Colors.transparent,
      key: Key("cpasswordFieldRegister"),
      child: TextFormField(
        obscureText: true,
        keyboardType: TextInputType.emailAddress,
        cursorColor: Colors.purple,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.lock, color: Colors.blue, size: 30),
            hintText: "Confirm password"),
        validator: (value) => value.isEmpty
            ? "Enter your password"
            : null,
        onChanged: (value) {
          _cpasswordField = value;
          updateButton();
        },
      ),
    );
  }

  Widget buttonConfirm() {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      onPressed: () async {
        if (_emailField == "" || _passwordField == "" || _nameField == "")
          return setState(() { validCredentials = true;});

        if (_passwordField != _cpasswordField)
          return setState(() { validCredentials = true;});

        var request = Api();
        if (await request.signup(_emailField, _passwordField, _nameField, dropdownValue))
           Navigator.pushNamedAndRemoveUntil(context, "/home", (r) => false);
        else 
          return setState(() { validCredentials = true;});
      },
      textColor: Colors.white,
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
        width: _width / 2.75,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          gradient: LinearGradient(
              colors: <Color>[Colors.deepPurple, Colors.purple]),
        ),
        padding: const EdgeInsets.all(15.0),
        child: Text(
          "Create",
          style: TextStyle(fontSize: 12),
        ),
      ),
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    final regex = RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Enter Valid Email';
    } else {
      return null;
    }
  }
}
