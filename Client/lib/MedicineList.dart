import 'package:Client/Singleton.dart';
import 'package:flutter/material.dart';
import 'package:Client/Api.dart';


class MedecineScreen extends StatefulWidget {
  MedecineScreen({Key key}) : super(key: key);

  @override
  MedecineScreenState createState() => MedecineScreenState();
}

class MedecineScreenState extends State<MedecineScreen> {

 @override
  void initState() {
    super.initState();
  }
  var singleton = Singleton();
  Map<String, dynamic> medicine;

    @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getMedicineUser(),
      builder: (context, snapshot) {
        return Scaffold(
          appBar: AppBar( title: Text("Medicine"),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.power_settings_new),
                onPressed: () {
                   singleton.clear();
                   Navigator.pushNamedAndRemoveUntil(context, '/login', (r) => false);
                },
              ),
            ],),
          body: medicine == null ? Center(child: CircularProgressIndicator(backgroundColor: Colors.greenAccent,)) :
            SingleChildScrollView(child: Container(
              margin: const EdgeInsets.only(top: 25.0),
              child: Center(child: Column(children: medicineInCard() ,),)
            ),),
        );
      },
    );
  }

  Future<void> getMedicineUser() async {
    var request = Api();
    medicine = await request.getMedicine(singleton.uid);
    //print(medicine.toString());
  }

  List<Card> medicineInCard() {
    List<Card> cards = [];

    for (int i = 0 ; i < medicine["data"].length ; i++)
      cards.add(Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        child: Container(
          width: MediaQuery.of(context).size.width - 40,
          height: MediaQuery.of(context).size.height / 9,
          child: ListTile(
            leading: Icon(Icons.healing, size: 70, color: Colors.grey),
            title: Text(medicine["data"][i]["name"]),
            trailing: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () async {
                        var request = Api();
                        await request.deleteMedicine(singleton.uid, medicine["data"][i]["id"]);
                        setState(() {});
                      },
                    ),
            ]),
          )
        )
      ));
    return cards;
  }
}