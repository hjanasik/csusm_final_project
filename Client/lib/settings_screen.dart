import 'package:Client/Singleton.dart';
import 'package:flutter/material.dart';

class SettingScreen extends StatefulWidget {
  SettingScreen({Key key}) : super(key: key);

  @override
  SettingScreenState createState() => SettingScreenState();
}

class SettingScreenState extends State<SettingScreen> {

 @override
  void initState() {
    super.initState();
  }
  var singleton = Singleton();
  double _height;
  double _width;
  bool isSwitched = false;
  Color colorGender;


    @override
  Widget build(BuildContext context) {
    if (singleton.gender == "Other")
      colorGender = Colors.grey;
    else if (singleton.gender == "Woman")
      colorGender = Colors.pink;
    else if (singleton.gender == "Man")
      colorGender = Colors.blue;
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.power_settings_new),
              onPressed: () {
                 singleton.clear();
                 Navigator.pushNamedAndRemoveUntil(context, '/login', (r) => false);
              },
            ),
          ],
      ),
      body: Container(
        margin: EdgeInsets.only(
          left: _width / 10.0, right: _width / 10.0, top: _height / 20.0, bottom: _height / 10),
          height: _height,
          width: _width / 1,
        child: Column(
          children: [
            CircleAvatar(child:  Tooltip(message: 'My Profile', child: FlatButton(child: Icon(Icons.person, size: 100, color: colorGender,), onPressed: () {},),), radius: 80,),
            Container(
              decoration: BoxDecoration(color: Colors.lightBlueAccent, border: Border.all(color: Colors.grey, width: 3), borderRadius: BorderRadius.circular(20)),
              margin: EdgeInsets.only(top: 40),
              height: _height / 4,
              width: _width / 1,
              child: Column(children: [
                SizedBox(height: 20,),
                /* NAME */
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text("Name: ", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                  Text(singleton.name, style: TextStyle(fontSize: 20),),
                ],),
                SizedBox(height: 20,),
                /* EMAIL */
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text("Email: ", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                  Text(singleton.email, style: TextStyle(fontSize: 20)),
                ],),
                SizedBox(height: 20,),
                /* GENDER */
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text("Gender: ", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                  Text(singleton.gender, style: TextStyle(fontSize: 20)),
                ],),
              ],),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              height: _height / 15,
              width: _width / 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Show Notification ", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),),
                  Switch(
                    value: singleton.isSwitch,
                    onChanged: (value) {
                      setState(() { singleton.isSwitch = value; });
                      Scaffold.of(context).showSnackBar(SnackBar(
                        backgroundColor: Colors.deepPurple,
                        content: value == true ? Text("Notification On"): Text("Notification Off"),
                        duration: Duration(milliseconds: 500),
                      ));
                    }
                  )
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}