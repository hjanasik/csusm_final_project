import 'dart:convert';
import 'package:Client/Singleton.dart';
import 'package:http/http.dart' as http;

class Api {

  String url;

  Api() {
    url = "http://192.168.1.41:1337";
  }

  Future<bool> signin(email, password) async {
    final http.Response response = await http.post(
      url + '/account/login',
      headers: <String, String> {
        'Content-Type': "application/json; charset=UTF-8",
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
      })
    );
    Map<String, dynamic> res = jsonDecode(response.body);
    if (res['code'] == "OK") {
      var singleton = Singleton();
      singleton.uid = res['data']['uid'];
      singleton.name = res['data']['data']['name'];
      singleton.gender = res['data']['data']['gender'];
      singleton.email = res['data']['data']['email'];
      return true;
    }
    return false;
  }

  Future<bool> signup(email, password, name, gender) async {
    final http.Response response = await http.post(
      url + '/account/register',
      headers: <String, String> {
        'Content-Type': "application/json; charset=UTF-8",
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
        'name': name,
        'gender': gender,
      })
    );
    Map<String, dynamic> res = jsonDecode(response.body);
    if (res['code'] == "OK") {
      var singleton = Singleton();
      singleton.uid = res['data']['uid'];
      singleton.name = res['data']['data']['name'];
      singleton.gender = res['data']['data']['gender'];
      singleton.email = res['data']['data']['email'];
      return true;
    }
    return false;
  }

  Future<Map<String, dynamic>> getMedicine(uid) async {
    final http.Response response = await http.get(url + '/medicine?uid=' + uid);
    Map<String, dynamic> res = jsonDecode(response.body);
    return res;
  }

  void addMedicineUser(cip) async {
     final http.Response response = await http.post(
      url + '/medicine',
      headers: <String, String> {
        'Content-Type': "application/json; charset=UTF-8",
      },
      body: jsonEncode(<String, String>{
        'uid': Singleton().uid,
        'cip': cip
      })
    );
   Map<String, dynamic> res = jsonDecode(response.body);
    if (res['code'] == "OK") {
      return;
    }
    throw "Error";
  }

  Future<void> deleteMedicine(uid, id) async {
    await http.post(
      url + '/deleteMedicine',
      headers: <String, String> {
        'Content-Type': "application/json; charset=UTF-8",
      },
      body: jsonEncode(<String, String>{
        'uid': Singleton().uid,
        'medicine_id': id
      })
    );
    return;
  }

}