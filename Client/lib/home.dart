import 'package:flutter/material.dart';
import 'package:Client/settings_screen.dart';
import 'package:Client/Welcome.dart';
import 'package:Client/MedicineList.dart';
import 'package:Client/camera.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<Home> {

  final camera = Camera();

  //////NAVIGATION VARIABLES/////
  Map<String, Widget> navigationItemBottomBar = const {
    "Home": Icon(Icons.home),
    "Medicines": Icon(Icons.healing),
    "Profile": Icon(Icons.person)
  };
  Map<int, Widget> navigationBody = {
    0: WelcomeScreen(),
    1: MedecineScreen(),
    2: SettingScreen(),
  };
  int curretnIndex = 0;
  /////////////////////////////


 @override
  void initState() {
    super.initState();
  }

  void updateIndex(int index) {
    setState(() { curretnIndex = index; });
  }

    @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: navigationBody[curretnIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: curretnIndex,
        elevation: 0.0,
        onTap: updateIndex,
        selectedItemColor: Colors.purple,
        unselectedItemColor: Colors.black,
        items: navigationItemBottomBar.entries.map((e) => 
          BottomNavigationBarItem(title: Text(e.key), icon: e.value),
        ).toList(),
      ),
    );
  }
}