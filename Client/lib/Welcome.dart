import 'package:Client/camera.dart';
import 'package:flutter/material.dart';
import 'package:Client/Api.dart';
import 'package:Client/scannedMedicine.dart';
import 'package:Client/Singleton.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key key}) : super(key: key);

  @override
  WelcomeScreenState createState() => WelcomeScreenState();
}

class WelcomeScreenState extends State<WelcomeScreen> {

    final camera = Camera();
    var singleton = Singleton();

 @override
  void initState() {
    super.initState();
  }

    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        actions: <Widget>[
              IconButton(
                icon: Icon(Icons.power_settings_new),
                onPressed: () {
                   singleton.clear();
                   Navigator.pushNamedAndRemoveUntil(context, '/login', (r) => false);
                },
              ),
            ],
          ),
      body: Center(
        child: Text("Tap the button to add a medicine to your account", textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 24),),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Scan',
        child: Icon(Icons.camera_alt),
        onPressed: () async {
          var content = await camera.rawContent;
          if (content != '') {
            try {
              var scannedMedicine = ScannedMedicine.fromDataMatrix(content);
              var request = Api();
              request.addMedicineUser(scannedMedicine.cip);
              print("CIP = " + scannedMedicine.cip);
            } catch (error) {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("A problem occur", style: const TextStyle(color: Colors.black)),
                    content: Text(error, style: const TextStyle(color: Colors.black)),
                  );
                }
              );
            }
          }
        },
      ),
    );
  }
}