class Singleton {
  static final Singleton _singleton = Singleton._internal();

  factory Singleton() {
    return _singleton;
  }

  void clear() {
    uid = "";
    name = "";
    gender = "";
    email = "";
  }

  String uid;
  String name;
  String gender;
  String email;
  bool isSwitch = false;

  Singleton._internal();
}