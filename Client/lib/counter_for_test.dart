import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Counter {
  int value = 0;

  void increment() => value++;

  void decrement() => value--;
}

class WidgetToTest extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber,
      child: Column(
        children: [
          Text("This is a widget"),
          SizedBox(height: 20),
          FlutterLogo(),
        ],
      ));
  }
}