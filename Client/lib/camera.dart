


////////// ACTUALLY CODE FROM GOOGLE ////////////


import 'package:barcode_scan/barcode_scan.dart';
import 'package:barcode_scan/model/scan_options.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Camera {
  Camera();
  ScanResult scanResult;

  final _flashOnController = TextEditingController(text: "Flash on");
  final _flashOffController = TextEditingController(text: "Flash off");
  final _cancelController = TextEditingController(text: "Cancel");

  final _aspectTolerance = 0.00;
  final _numberOfCameras = numberOfCameras();
  final _selectedCamera = -1;
  final _useAutoFocus = true;
  final _autoEnableFlash = false;

  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

  List<BarcodeFormat> selectedFormats = [..._possibleFormats];

  Future<String> get rawContent => scan();
  Future<int> get numberOfCamera => _numberOfCameras;
  static Future numberOfCameras() async {
    return await BarcodeScanner.numberOfCameras;
  }

  Future<String> scan() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": _cancelController.text,
          "flash_on": _flashOnController.text,
          "flash_off": _flashOffController.text,
        },
        restrictFormat: selectedFormats,
        useCamera: _selectedCamera,
        autoEnableFlash: _autoEnableFlash,
        android: AndroidOptions(
          aspectTolerance: _aspectTolerance,
          useAutoFocus: _useAutoFocus,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);
      return result.rawContent;
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        result.rawContent = 'The user did not grant the camera permission!';
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      return result.rawContent;
    }
  }
}
