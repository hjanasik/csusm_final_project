# Client

A flutter application. The flutter is part of a CSUSM Project


# Installation
Please first install [Flutter / Dart](https://flutter.dev/docs/get-started/install) on your computer.

# Usage

Go to the Client folder
```bash
cd Client
```

Download depedencies
```bash
flutter pub get
```

Launch the client through Vscode or AndroidStudio

## License
[MIT](https://choosealicense.com/licenses/mit/)