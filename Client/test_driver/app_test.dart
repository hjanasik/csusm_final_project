import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_driver/flutter_driver.dart';

void main() {
  group('Client App', () {

    final _emailForm = null;//find.byValueKey("emailFieldRegister");
    final _passwordForm = null;//find.byValueKey("passwordFieldRegister");
    final _cpasswordForm = null;//find.byValueKey("cpasswordFieldRegister");

    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('email field in register page empty at start', () async {
      expect(_emailForm.toString(), "");
    });

    test('password field in register page empty at start', () async {
      expect(_passwordForm.toString(), "");
    });

    test('confirm password field in register page empty at start', () async {
      expect(_cpasswordForm.toString(), "");
    });

  });
}

